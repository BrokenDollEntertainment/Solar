#ifndef SOLAR_ENTITY_HPP
#define SOLAR_ENTITY_HPP

#include <typeinfo>
#include <unordered_map>
#include "Solar/AEntityScript.hpp"
#include "Solar/Component.hpp"
#include "Solar/Indexer.hpp"
#include "Solar/TemplateContainer.hpp"

namespace Solar
{
    class AEntityScript;
    class InnerCore;
    class Scene;
    class System;
    class Entity
    {
        friend class InnerCore;
        friend class Scene;
        friend class System;
    private:
        Solar::Indexer::indexType idx;
        std::string id;
        TemplateContainer<Component> components;
        std::weak_ptr<InnerCore> core;
        std::weak_ptr<Entity> own;
        std::weak_ptr<Scene> scene;
        TemplateContainer<AEntityScript> scripts;

    private:
        Entity(const std::weak_ptr<InnerCore> &, const std::string &);
        void setUid(Indexer::indexType uid);
        void setOwn(const std::weak_ptr<Entity> &);
        void init(const std::weak_ptr<Scene> &);
        void updateScene();
        bool find(const std::string &);
        const std::weak_ptr<Scene> &getScene() const;

    public:
        Indexer::indexType getIdx() const;
        void destroy();
        const std::string &getId() const;
        void add(const std::shared_ptr<AEntityScript> &);
        void add(const std::shared_ptr<Component> &);

        template <typename T, typename... Args>
        void add(Args&&... args)
        {
            if (std::is_base_of<Component, T>::value)
                add(std::dynamic_pointer_cast<Component>(std::make_shared<T>(std::forward<Args>(args)...)));
            else if (std::is_base_of<AEntityScript, T>::value)
                add(std::dynamic_pointer_cast<AEntityScript>(std::make_shared<T>(std::forward<Args>(args)...)));
        }

        template<typename T>
        bool find()
        {
            if (std::is_base_of<Component, T>::value)
                return components.find<T>();
            else if (std::is_base_of<AEntityScript, T>::value)
                return scripts.find<T>();
            return false;
        }

        template<typename T>
        const std::shared_ptr<T> get() const
        {
            if (std::is_base_of<Component, T>::value)
                return components.get<T>();
            else if (std::is_base_of<AEntityScript, T>::value)
                return scripts.get<T>();
            return nullptr;
        }

        template<typename T>
        void remove()
        {
            if (std::is_base_of<Component, T>::value)
            {
                if (components.remove<T>())
                    updateScene();
            }
            else if (std::is_base_of<AEntityScript, T>::value)
                scripts.remove<T>();
        }

        virtual ~Entity() = default;
    };
}

#endif