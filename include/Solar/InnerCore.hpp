#ifndef SOLAR_INNERCORE_HPP
#define SOLAR_INNERCORE_HPP

#include "Solar/Entity.hpp"
#include "Solar/Indexer.hpp"
#include "Solar/World.hpp"
#include "Solar/Scene.hpp"

namespace Solar
{
    class Core;
    /**
     * @brief Class used by the Core
     *
     * Private class used by the Core to manage the ECS
     */
    class InnerCore final
    {
        friend class Core;
        friend class Entity;
        friend class World;
    private:
        Indexer indexer;
        std::weak_ptr<InnerCore> own;
        std::shared_ptr<World> primalWorld;
        std::shared_ptr<World> currentWorld;
        std::unordered_map<std::string, std::shared_ptr<World>> worlds;
        std::vector<std::shared_ptr<Scene>> scenes;

    private:
        InnerCore() = default;
        void goToWorld(const std::string &name);
        const std::shared_ptr<World> &getPrimalWorld() const;
        void setOwn(const std::weak_ptr<InnerCore> &);
        std::shared_ptr<Entity> entity(const std::string &);
        std::shared_ptr<World> world(const std::string &);
        std::shared_ptr<Scene> scene(const std::string &);
        void update(size_t);
        void destroy(const std::weak_ptr<Entity> &);

    public:
        /**
         * @brief Destructor
         *
         * Destroy the InnerCore and clear his memory
         */
        ~InnerCore();
    };
}

#endif