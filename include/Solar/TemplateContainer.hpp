#ifndef SOLAR_TEMPLATECONTAINER_HPP
#define SOLAR_TEMPLATECONTAINER_HPP

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

namespace Solar
{
    template <typename U>
    class TemplateContainer
    {
    private:
        std::unordered_map<std::string, std::shared_ptr<U>> elements;

    public:
        template <typename T>
        void add(const std::shared_ptr<T> &element)
        {
            if (!std::is_base_of<U, T>::value)
                return;
            elements[typeid(*element).name()] = std::dynamic_pointer_cast<U>(element);
        }

        template <typename T, typename... Args>
        void add(Args&&... args)
        {
            if (!std::is_base_of<U, T>::value)
                return;
            elements[typeid(T).name()] = std::dynamic_pointer_cast<U>(std::make_shared<T>(std::forward<Args>(args)...));
        }

        void clear()
        {
            elements.clear();
        }

        template<typename T>
        bool find() const
        {
            return find(typeid(T).name());
        }

        bool find(const std::string &name) const
        {
            return (elements.find(name) != elements.end());
        }

        template<typename T>
        const std::shared_ptr<T> get() const
        {
            std::string templateName = typeid(T).name();
            if (elements.find(templateName) != elements.end())
                return std::dynamic_pointer_cast<T>(elements.at(templateName));
            return nullptr;
        }

        const std::shared_ptr<U> get(const std::string &name) const
        {
            if (elements.find(name) != elements.end())
                return elements.at(name);
            return nullptr;
        }

        template<typename T>
        bool remove()
        {
            if (elements.find(typeid(T).name()) == elements.end())
                return false;
            elements.erase(typeid(T).name());
            return true;
        }

        bool remove(const std::string &name)
        {
            if (elements.find(name) == elements.end())
                return false;
            elements.erase(name);
            return true;
        }

        void iterate(const std::function<void(const std::shared_ptr<U> &)> &fct) const
        {
            for (const auto &element : elements)
                fct(element.second);
        }
        const std::shared_ptr<U> operator[](const std::string &name) const
        {
            return get(name);
        }

        unsigned long size() const
        {
            return elements.size();
        }
    };
}

#endif
