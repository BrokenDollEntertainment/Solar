#ifndef SOLAR_ASCENESCRIPT_HPP
#define SOLAR_ASCENESCRIPT_HPP

#include "Solar/ScriptPtr.hpp"
#include "Solar/System.hpp"

namespace Solar
{
    class Scene;
    class ASceneScript
    {
        friend class Scene;
    private:
        void setScene(const std::weak_ptr<Scene> &);

    protected:
        ScriptPtr<Scene> scene;

    protected:
        virtual void receive(const System::Event &, const std::shared_ptr<Solar::Entity> &);
        virtual void receive(const System::Event &);
        virtual void init();
        virtual void destroy();
        virtual void update(size_t);
        virtual ~ASceneScript() = default;
    };
}

#include "Solar/Scene.hpp"

#endif