#ifndef SOLAR_AWORLDSCRIPT_HPP
#define SOLAR_AWORLDSCRIPT_HPP

#include "Solar/ScriptPtr.hpp"

namespace Solar
{
    class World;
    class AWorldScript
    {
        friend class World;
    private:
        void setWorld(const std::weak_ptr<World> &);

    protected:
        ScriptPtr<World> world;

    protected:
        virtual void init();
        virtual void destroy();
        virtual void update(size_t);
        virtual ~AWorldScript() = default;
    };
}

#include "Solar/World.hpp"

#endif