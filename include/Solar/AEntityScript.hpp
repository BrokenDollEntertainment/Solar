#ifndef SOLAR_AENTITYSCRIPT_HPP
#define SOLAR_AENTITYSCRIPT_HPP

#include "Solar/ScriptPtr.hpp"

namespace Solar
{
    class Entity;
    class World;
    /**
     * Class allowing to specify some action of the Entity
     */
    class AEntityScript
    {
        friend class Entity;
    protected:
        ScriptPtr<Entity> entity;

    private:
        void setEntity(const std::weak_ptr<Entity> &);
        void destroyEntity();

    public:
        virtual void init();
        virtual void destroy();
        virtual ~AEntityScript() = default;
    };
}

#include "Solar/Entity.hpp"

#endif