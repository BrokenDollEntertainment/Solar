#ifndef SOLAR_VECTOR_HPP
#define SOLAR_VECTOR_HPP

#include <vector>
#include "Solar/Entity.hpp"

namespace Solar
{
    class Scene;
    class System;
    template <typename T>
    class Vector final
    {
        friend class Scene;
        friend class System;
    public:
        typedef std::shared_ptr<T> *iterator;
        typedef const std::shared_ptr<T> *const_iterator;

    private:
        unsigned long vectorSize;
        std::vector<std::shared_ptr<T>> items;

    private:
        Vector() : vectorSize(0) {}

        void optimizeFor(unsigned int size)
        {
            items.reserve(size);
        }

        int getPos(const std::shared_ptr<T> &item)
        {
            int left = 0;
            auto right = static_cast<int>(vectorSize - 1);
            if (item->getIdx() < items[0]->getIdx() || item->getIdx() > items[right]->getIdx())
                return -1;
            while ((items[left]->getIdx() != items[right]->getIdx()) && (item->getIdx() > items[left]->getIdx()) && (item->getIdx() < items[right]->getIdx()))
            {
                auto pos = static_cast<int>(left + ((right - left) * ((item->getIdx() - items[left]->getIdx()) / (items[right]->getIdx() - items[left]->getIdx()))));
                if (items[pos]->getIdx() == item->getIdx())
                    return pos;
                else if(items[pos]->getIdx() < item->getIdx())
                    left = pos + 1;
                else
                    right = pos - 1;
            }
            if (items[left]->getIdx() == item->getIdx())
                return left;
            return -1;
        }

        void insert(const std::shared_ptr<T> &item, int pos)
        {
            items.insert(items.begin() + pos, item);
            ++vectorSize;
        }

    public:
        unsigned long size() const
        {
            return vectorSize;
        }

        bool add(const std::shared_ptr<T> &item)
        {
            if (vectorSize == 0)
            {
                insert(item, static_cast<int>(vectorSize));
                return true;
            }
            int left = 0;
            auto right = static_cast<int>(vectorSize - 1);
            if (item->getIdx() < items[0]->getIdx())
            {
                insert(item, 0);
                return true;
            }
            else if (item->getIdx() > items[right]->getIdx())
            {
                insert(item, static_cast<int>(vectorSize));
                return true;
            }
            while ((items[left] != items[right]) && (item > items[left]) && (item < items[right]))
            {
                auto pos = static_cast<int>(left + ((right - left) * ((item->getIdx() - items[left]->getIdx()) / (items[right]->getIdx() - items[left]->getIdx()))));
                if (items[pos]->getIdx() == item->getIdx())
                    return false;
                else if(items[pos]->getIdx() < item->getIdx())
                    left = pos + 1;
                else
                    right = pos - 1;
            }
            if (items[left]->getIdx() == item->getIdx())
                return false;
            if (items[left]->getIdx() > item->getIdx())
                insert(item, left);
            else if (items[right]->getIdx() < item->getIdx())
                insert(item, right + 1);
            return true;
        }

        bool remove(const std::shared_ptr<T> &item)
        {
            int pos = getPos(item);
            if (pos != -1)
            {
                items.erase(items.begin() + pos);
                --vectorSize;
                return true;
            }
            return false;
        }

        bool find(const std::shared_ptr<T> &item)
        {
            return (getPos(item) != -1);
        }

        void clear()
        {
            vectorSize = 0;
            items.clear();
            items.shrink_to_fit();
        }

        iterator begin()
        {
            return &items[0];
        }

        const_iterator begin() const
        {
            return &items[0];
        }

        iterator end()
        {
            return &items[vectorSize];
        }

        const_iterator end() const
        {
            return &items[vectorSize];
        }

        ~Vector()
        {
            items.clear();
            items.shrink_to_fit();
        }
    };
}

#endif