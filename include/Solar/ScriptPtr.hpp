#ifndef SOLAR_SCRIPTPTR_HPP
#define SOLAR_SCRIPTPTR_HPP

#include <iostream>
#include <memory>

namespace Solar
{
    template <typename T>
    class ScriptPtr
    {
    private:
        std::weak_ptr<T> ptr;

    public:
        void setPtr(const std::weak_ptr<T> &ptr)
        {
            this->ptr = ptr;
        }

        void reset()
        {
            ptr.reset();
        }

        T *operator->()
        {
            return ptr.lock().get();
        }

        ~ScriptPtr()
        {
            ptr.reset();
        }
    };
}

#endif