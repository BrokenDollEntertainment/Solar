# SOLAR
The Solar is an ECS (Entity Component System) in C++ created by Mewen LE RESTE. The Solar is under MIT License and is free to use and change


## Installation
To install the Solar library on Linux, clone the repository and use the command:<br>
`make install`


## Example
Here is a simple Hello World! example using the Solar:<br>
main.cpp
```
#include <iostream>
#include <Solar/Core.hpp>

class Printable : public Solar::Component
{
private:
    std::string text;

public:
    Printable(const std::string &text) : text(text) {}

    const std::string &getText() const
    {
        return text;
    }
};

class Printer : public Solar::System
{
public:
    Printer()
    {
        include<Printable>();
    }

    void run(const Solar::TimeIdx &) override
    {
        for (const auto &entity : entities)
        {
            auto printable = entity->get<Printable>();
            if (printable)
                std::cout << printable->getText() << std::endl;
        }
    }
};

int main()
{
    Solar::Core core;
    Solar::TimeIdx idx = {0, 0};
    auto scene = core.getPrimalWorld()->getDefaultScene();
    scene->add<Printer>();
    auto entity = core.createEntity("Entity");
    entity->add<Printable>("Hello World!");
    scene->add(entity);
    core.update(idx);
    return 0;
}
```
To compile this test on Linux use the command<br>
`g++ main.cpp -o solarTest -lSolar`


## License
The MIT License (MIT)

Copyright (c) 2018 Mewen LE RESTE

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.