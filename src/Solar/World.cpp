#include "Solar/AWorldScript.hpp"
#include "Solar/InnerCore.hpp"
#include "Solar/World.hpp"
#include "Solar/Scene.hpp"

Solar::World::World(const std::weak_ptr<InnerCore> &core, bool primal, const std::string &name, size_t idx) :
        primal(primal),
        idx(idx),
        core(core),
        name(name)
{
    defaultScene = std::shared_ptr<Scene>(new Scene(name));
    defaultScene->setOwn(defaultScene);
}

const std::shared_ptr<Solar::Scene> Solar::World::getScene(const std::string &name)
{
    for (const auto &subScene : scenes)
    {
        if (subScene->compare(name))
            return subScene;
    }
    return nullptr;
}

void Solar::World::addScript(const std::shared_ptr<AWorldScript> &script)
{
    script->setWorld(own);
    if (primal)
        script->init();
    scripts.add(script);
}

void Solar::World::replaceSceneBy(const std::weak_ptr<Solar::Scene> &scene, const std::shared_ptr<Solar::Scene> &with)
{
    auto toReplace = scene.lock();
    if (toReplace == defaultScene)
        return;
    for (unsigned int n = 0; n != scenes.size(); ++n)
    {
        if (scenes[n] == toReplace)
        {
            scenes[n] = with;
            return;
        }
    }
}

void Solar::World::update(size_t timeIdx)
{
    if (defaultScene->isActive())
        defaultScene->update(timeIdx);
    for (const auto &scene : scenes)
    {
        if (scene->isActive())
            scene->update(timeIdx);
    }
    scripts.iterate([=](auto script){script->update(timeIdx);});
}


const std::string &Solar::World::getName() const
{
    return name;
}

bool Solar::World::compare(const std::string &name)
{
    return (name == this->name);
}

void Solar::World::add(const std::shared_ptr<Scene> &toAdd)
{
    if (toAdd == defaultScene)
        return;
    auto sceneWorld = toAdd->getWorld().lock();
    if (sceneWorld && sceneWorld != own.lock())
        return;
    else if (!sceneWorld)
        toAdd->setWorld(own);
    for (const auto &subScene : scenes)
    {
        if (subScene == toAdd)
            return;
    }
    scenes.emplace_back(toAdd);
}

void Solar::World::load()
{
    if (primal)
        return;
    scripts.iterate([](auto script){script->init();});
    defaultScene->load();
    for (const auto &scene : scenes)
        scene->load();
}

void Solar::World::unload()
{
    if (primal)
        return;
    scripts.iterate([](auto script){script->destroy();});
    defaultScene->unload();
    for (const auto &scene : scenes)
        scene->unload();
}

void Solar::World::setOwn(const std::weak_ptr<Solar::World> &own)
{
    this->own = own;
    defaultScene->setWorld(own);
}

bool Solar::World::isPrimal() const
{
    return primal;
}

size_t Solar::World::getIdx() const
{
    return idx;
}

const std::shared_ptr<Solar::Scene> &Solar::World::getDefaultScene() const
{
    return defaultScene;
}

Solar::World::~World()
{
    core.reset();
    scripts.iterate([](auto script){script.reset();});
    own.reset();
    defaultScene.reset();
    scenes.clear();
    scenes.shrink_to_fit();
}
