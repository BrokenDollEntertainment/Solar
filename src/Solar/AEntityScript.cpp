#include "Solar/AEntityScript.hpp"

void Solar::AEntityScript::setEntity(const std::weak_ptr<Entity> &entity)
{
    this->entity.setPtr(entity);
}

void Solar::AEntityScript::destroyEntity()
{
    destroy();
    entity.reset();
}

void Solar::AEntityScript::init() {}
void Solar::AEntityScript::destroy() {}