#include "Solar/AWorldScript.hpp"

void Solar::AWorldScript::setWorld(const std::weak_ptr<World> &world)
{
    this->world.setPtr(world);
}

void Solar::AWorldScript::init() {}
void Solar::AWorldScript::destroy() {}
void Solar::AWorldScript::update(size_t) {}