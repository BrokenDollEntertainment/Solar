#include "Solar/Core.hpp"

std::shared_ptr<Solar::InnerCore> Solar::Core::core = nullptr;
unsigned int Solar::Core::instance = 0;

Solar::Core::Core()
{
    if (!core)
    {
        core = std::shared_ptr<InnerCore>(new InnerCore());
        core->setOwn(core);
    }
    ++instance;
}

void Solar::Core::goToWorld(const std::string &name)
{
    core->goToWorld(name);
}

const std::shared_ptr<Solar::World> &Solar::Core::getPrimalWorld() const
{
    return core->getPrimalWorld();
}

std::shared_ptr<Solar::Entity> Solar::Core::entity(const std::string &name) const
{
    return core->entity(name);
}

std::shared_ptr<Solar::World> Solar::Core::world(const std::string &name) const
{
    return core->world(name);
}

std::shared_ptr<Solar::Scene> Solar::Core::scene(const std::string &name) const
{
    return core->scene(name);
}

void Solar::Core::update(size_t timeIdx)
{
    core->update(timeIdx);
}

Solar::Core::~Core()
{
    --instance;
    if (instance == 0)
        core.reset();
}
