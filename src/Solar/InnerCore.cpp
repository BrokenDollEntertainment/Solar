#include "Solar/InnerCore.hpp"

void Solar::InnerCore::setOwn(const std::weak_ptr<InnerCore> &own)
{
    this->own = own;
    primalWorld = std::shared_ptr<World>(new World(own, true, "primal", worlds.size()));
    primalWorld->setOwn(primalWorld);
}

std::shared_ptr<Solar::Entity> Solar::InnerCore::entity(const std::string &name)
{
    auto newEntity = std::shared_ptr<Entity>(new Entity(own, name));
    newEntity->setOwn(newEntity);
    newEntity->setUid(indexer.next());
    return newEntity;
}

std::shared_ptr<Solar::World> Solar::InnerCore::world(const std::string &name)
{
    if (worlds.find(name) == worlds.end())
    {
        auto newWorld = std::shared_ptr<World>(new World(own, false, name, worlds.size()));
        newWorld->setOwn(newWorld);
        worlds[name] = newWorld;
        return newWorld;
    }
    return worlds[name];
}

std::shared_ptr<Solar::Scene> Solar::InnerCore::scene(const std::string &name)
{
    auto newScene = std::shared_ptr<Scene>(new Scene(name));
    newScene->setOwn(newScene);
    scenes.emplace_back(newScene);
    return newScene;
}

void Solar::InnerCore::update(size_t timeIdx)
{
    if (currentWorld)
        currentWorld->update(timeIdx);
    primalWorld->update(timeIdx);
}

const std::shared_ptr<Solar::World> &Solar::InnerCore::getPrimalWorld() const
{
    return primalWorld;
}

void Solar::InnerCore::goToWorld(const std::string &name)
{
    if (worlds.find(name) == worlds.end())
        return;
    auto worldToSet = worlds[name];
    currentWorld->unload();
    worldToSet->load();
    currentWorld = worldToSet;
}

void Solar::InnerCore::destroy(const std::weak_ptr<Solar::Entity> &entity)
{
    auto toDestroy = entity.lock();
    indexer.free(toDestroy->getIdx());
    for (const auto &scene : scenes)
        scene->remove(toDestroy);
}

Solar::InnerCore::~InnerCore()
{
    own.reset();
    primalWorld.reset();
    if (currentWorld != nullptr)
        currentWorld.reset();
    worlds.clear();
    scenes.clear();
    scenes.shrink_to_fit();
}
